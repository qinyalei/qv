# -*- coding:utf-8 -*-
import re,os,fnmatch
import numpy as np

class qv_model( ):
    
    def __init__(self,source):
        self.source = source
        self.reg = []
        self.wire = []
        self.analyse()

    def analyse(self):
        parameter_compile = r'^\s*P\s*:\s*(.*)$'
        input_compile = r'^\s*I\s*:\s*(.*)$'
        output_compile = r'^\s*O\s*:\s*(.*)$'
        always_ff_compile = r'^\s*(\w+)\s*\(\s*(\w+)\s*,\s*(\w+)\s*\)\s*=\s*(.*)$'
        always_ff2_compile = r'^\s*(\w+)\s*\(\s*(\w+)\s*,\s*(\w+)\s*,\s*(\w+)\s*\)\s*=\s*(.*)$'
        always_comb_compile = r'^\s*(\w+)\s*\(\s*(\w+)\s*\)\s*=\s*(.*)$'
        instant_compile = r'^\s*(\w+)\s*::\s*(\w+)(.*)$'

        parameter_l   = re.findall(parameter_compile  ,self.source,re.M)
        input_l       = re.findall(input_compile      ,self.source,re.M)
        output_l      = re.findall(output_compile     ,self.source,re.M)
        always_ff_l   = re.findall(always_ff_compile  ,self.source,re.M)
        always_ff2_l   = re.findall(always_ff2_compile,self.source,re.M)
        always_comb_l = re.findall(always_comb_compile,self.source,re.M)
        instant_l     = re.findall(instant_compile    ,self.source,re.M  )

        self.parameter      = self.parameter_tran(parameter_l)
        self.input          = self.input_tran(input_l)
        self.output         = self.output_tran(output_l)
        self.always_ff      = self.always_ff_tran(always_ff_l)
        self.always_ff2     = self.always_ff_tran(always_ff2_l)
        self.always_comb    = self.always_comb_tran(always_comb_l)
        self.instant        = self.instant_tran(instant_l)


    def parameter_tran(self,parameter_l):
        out = {}
        r0 = np.empty([0,2],dtype='<U64')
        for t0 in parameter_l:
            r0 = np.insert(r0,r0.shape[0],np.array(re.findall(r'(\w+)\s*:\s*(.+)',t0)),axis=0 )
        out['list'] = r0

        out['text'] = ',\n'.join( '\tparameter %s = %s'%(r0[i,0],r0[i,1]) for i in range(r0.shape[0])  )

        return out

    def input_tran(self,input_l): 
        out = {}   
        r0 = np.empty([0,2],dtype='<U32')
        for t0 in input_l:
            r0 = np.insert(r0,r0.shape[0],np.array(re.findall(r'(\w+)\s*,?\s*(\w+)',t0)),axis=0 )
        out['list'] = r0
        out['text'] = ',\n'.join( '\tinput [%s:0] %s'%(r0[i,0]+'-1',r0[i,1]) for i in range(r0.shape[0])  )
        return out

    def output_tran(self,output_l):  
        out = {}  
        r0 = np.empty([0,2],dtype='<U32')
        for t0 in output_l:
            r0 = np.insert(r0,r0.shape[0],np.array(re.findall(r'(\w+)\s*,?\s*(\w+)',t0)),axis=0 )
        out['list'] = r0
        out['text'] = ',\n'.join( '\toutput [%s:0] %s'%(r0[i,0]+'-1',r0[i,1]) for i in range(r0.shape[0])  )
        return out

    def always_ff_tran(self,always_ff_l):
        out = {}
        out['list'] = np.array(always_ff_l)

        t0 = ''
        for r0 in always_ff_l:
            if len(r0) == 5:
                rst_en = 1
            else :
                rst_en = 0
            if r0[-1].find('??') >= 0 :
                t0 += self.always_case_text( r0 ,rst_en )
            else:
                t0 += self.always_ff_text( r0 , rst_en )
        out['text'] = t0
        return out
            
    def always_ff_text(self, r0 , rst_en ):
        if rst_en:
            t0 = 'always @ ( posedge %s or negedge %s ) begin \n' %(r0[2],r0[3])
            r1 = r0[4].split(';')
        else:
            t0 = 'always @ ( posedge %s ) begin \n' %(r0[2])
            r1 = r0[3].split(';')
        for index in range(len(r1)):
            if index == 0:
                tt = ''
            else :
                tt = 'else '
            rr = r1[index]
            r2 = rr.split('?')
            if len(r2) > 1:
                t0 += f'\t{tt}if ( {r2[0].strip()} ) begin \n\t\t{r0[0]} <= {r2[1].strip()} ;\n\tend\n'
            elif index == 0:
                if len(r1) > 1:
                    print('error:%s'%r0)
                    break
                t0 += f'\t{r0[0]} <= {r2[0].strip()} ;\n'
            else:
                t0 += f'\t{tt} begin \n\t\t{r0[0]} <= {r2[0].strip()} ;\n\tend\n'
        t0 += 'end\n\n'
        self.reg.append([r0[1],r0[0]])
        return t0

    def always_case_text(self, r0 ,rst_en ):
        if rst_en:
            t0 = 'always @ ( posedge %s or negedge %s ) begin \n' %(r0[2],r0[3])
            r1 = r0[4].split('??')
        else:
            t0 = 'always @ ( posedge %s ) begin \n' %(r0[2])
            r1 = r0[3].split('??')
        t0 += f'\tcase( {r1[0]} )\n'
        r2 = re.findall(r'([\w\[\]:\']+)\s*;\s*([\w\[\]:\']+)',r1[1])

        for r3 in r2 :
            rr = r3[1]
            t0 += f'\t\t{r3[0]}: begin \n\t\t\t{r0[0]} <= {rr} ;\n\t\tend\n'
        t0 += '\tendcase\nend\n\n'
        self.reg.append([r0[1],r0[0]])
        return t0

    def instant_tran(self,instant_l):
        out = {}
        out['list'] = np.array(instant_l)
        return out

    def always_comb_tran(self,always_comb_l):
        out = {}
        out['list'] = np.array(always_comb_l)
        t0=''
        for r0 in always_comb_l:
            t0 += self.always_comb_text(r0)
        out['text'] = t0
        return out

    def always_comb_text(self,r0):
        t0 = 'always @ ( * ) begin \n' 
        r1 = r0[2].split(';')
        for index in range(len(r1)):
            if index == 0:
                tt = ''
            else :
                tt = 'else '
            r2 = r1[index].split('?')
            if len(r2) > 1:
                t0 += f'\t{tt}if ( {r2[0].strip()} ) begin \n\t\t{r0[0]} = {r2[1].strip()} ;\n\tend\n'
            elif index == 0:
                if len(r1) > 1:
                    print('error:%s'%r0)
                    break
                t0 += f'\t{r0[0]} = {r2[0].strip()} ;\n'
            else:
                t0 += f'\t{tt} begin \n\t\t{r0[0]} = {r2[0].strip()} ;\n\tend\n'
        t0 += 'end\n\n'
        self.reg.append([r0[1],r0[0]])
        return t0


class qv( ):

    def __init__(self,in_file,out_path):
        self.word_dir = os.path.dirname(in_file)
        self.gen_source(in_file.replace('\\','/'))
        self.list_model()
        self.list_lib()
        self.gen_verilog(out_path.replace('\\','/'))

    def gen_source(self,in_file):
        print(in_file)
        fid = open(in_file,'r')
        self.source = fid.read() 
        fid.close()

        lib_compile = r'\s*`include\s*(\S+)'
        lib_list = re.findall(lib_compile,self.source,re.S)
        for lib_path in lib_list:
            fid = open( os.path.join( self.word_dir , lib_path)  ,'r')
            self.source += fid.read() 
            fid.close()


    def list_model(self):
        model_compile = r'\s*M\s*:\s*(\w+)\s*\{\{(.*?)\}\}'
        model_list = re.findall(model_compile,self.source,re.S)
        self.model_info = []
        for index in range(len(model_list)):
            info = []
            info.append(model_list[index][0])
            info.append( qv_model(model_list[index][1]) )

            self.model_info.append(info)

    def list_lib(self):
        lib_compile = r'\s*`lib\s*(\S+)'
        lib_list = re.findall(lib_compile,self.source,re.S)
        self.lib_info = []
        for lib_path in lib_list:
            for file_name in  self.iterfindfiles(os.path.join( self.word_dir , lib_path)):
                self.lib_info.append( self.get_v_info(file_name) )

    def iterfindfiles(self,path="/",fnexp1="*",fnexp2="*.v"):
        for root, dirs, files in os.walk(path):    
            for filename in fnmatch.filter(files, fnexp2):
                if fnmatch.fnmatch(root,fnexp1):   
                    if filename.find('_sim_netlist.v') == -1 and root.find( '\\sim' ) == -1  and root.find( '\\simulation' )== -1:
                        yield os.path.join(root, filename).replace('\\','/')


    def get_v_info(self,file_name):
        fid = open(file_name,'r', errors='ignore')
        a = fid.read().replace('\t','  ').replace(',',' ').replace(';',' ')
        fid.close()

        model_compile = r'^\s*module\s*(\w+).*$'
        v_info = re.findall(model_compile,a,re.M)

        parameter_compile = r'^\s*parameter\s*(\w+)\s*=.*$'
        v_info.append(re.findall(parameter_compile,a,re.M))

        input_compile = r'^\s*(input)\s*(wire)?\s*(\[[\s\w:+-/\*]*\])?\s*(\w+)'
        v_info.append(re.findall(input_compile,a,re.M))

        output_compile = r'^\s*(output)\s*(wire|reg)?\s*(\[[\s\w:+-/\*]*\])?\s*(\w+)'
        v_info.append(re.findall(output_compile,a,re.M))

        return v_info

    def gen_verilog(self,path):

        isExists=os.path.exists(path)
        if not isExists:
            print (path+' 创建成功')
            os.makedirs(path)
        else:
            print (path+' 目录已存在')


        for mode_info in self.model_info:
            file_name = mode_info[0]+'.v'
            print(f'\n-->开始生成 {file_name} 文件')
            model_text = self.gen_v_model_text(mode_info)
            full_path = os.path.join(path,file_name)
            if os.path.isfile(full_path):
                print(f'文件 {full_path} 将被覆盖')
            fid = open(full_path,'w')
            fid.write(model_text)
            fid.close
            print(f'-->文件 {full_path} 生成成功\n')



    def gen_v_model_text(self,mode_info):
        model_name = mode_info[0]
        text_instant,wire = self.gen_text_instant(mode_info[1].instant['list'])
        text_wirereg = self.gen_text_wirereg(mode_info[1],wire)

        text = f'module {model_name} '
        if mode_info[1].parameter['list'].shape[0] >0:
            text += '#(\n'+mode_info[1].parameter['text']+'\n)'

        text += '(\n' + mode_info[1].input['text'] +',\n'
        text += '\n' + mode_info[1].output['text'] +'\n);\n'
        text += '// wire reg define\n'
        text += text_wirereg
        text += '// instant define\n'
        text += text_instant
        text += mode_info[1].always_comb['text']
        text += mode_info[1].always_ff['text']
        text += mode_info[1].always_ff2['text']
        text += 'endmodule\n'

        return text


    def gen_text_instant(self,data):
        model_name_list = np.array(self.model_info)[:,0]
        if len(self.lib_info) > 0:
            lib_name_list = np.array(self.lib_info,dtype=object)[:,0]
        else:
            lib_name_list = np.empty([1,2])
        text=''
        wire = []
        for instant_index in range(data.shape[0]):
            model_index=np.where(model_name_list==data[instant_index,0])[0]
            lib_index=np.where(lib_name_list==data[instant_index,0])[0]
            compile_text = data[instant_index,2]

            if len(model_index) >0 :
                if len(model_index) >1 :
                    print('warning:qv model 重复 ， name：%s'%(data[instant_index,0]))

                input = self.model_info[model_index[0]][1].input['list']
                parameter = self.model_info[model_index[0]][1].parameter['list'][:,0]
                output = self.model_info[model_index[0]][1].output['list']

            elif len(lib_index) >0 :
                if len(lib_index) >1 :
                    print('warning:v lib 重复 ， name：%s'%(data[instant_index,0]))
                input = np.array(self.lib_info[lib_index[0]][2])[:,[2,3]]
                parameter = np.array(self.lib_info[lib_index[0]][1])
                output = np.array(self.lib_info[lib_index[0]][3])[:,[2,3]]

            else:
                print('warning:model 缺失 ， name：%s'%(data[instant_index,0]))
                input=np.array([])
                parameter=np.array([])
                output=np.array([])

            parameter_c = []
            if parameter.shape[0] == 0:
                parameter_text = ''
            else:
                parameter_text = '#(\n'
                
                for i in range(parameter.shape[0]):
                    a = self.re_do(parameter[i],compile_text)
                    if a != 'none':
                        if i != 0 :
                            parameter_text += ',\n'
                        parameter_text += f'\t.{parameter[i]}  ({a})'
                        parameter_c.append(a)
                    else:
                        parameter_c.append('')
                    if i == parameter.shape[0]-1 :
                        parameter_text += '\n) '
                    # else:
                    #     parameter_text += ',\n'

            port_text = '(\n'
            port_text += '\t// input\n'
            for i in range(input.shape[0]):
                a = self.re_do(input[i][1],compile_text)
                b = input[i][0]
                for iii in range(parameter.shape[0]) :
                    b = b.replace(parameter[iii],parameter_c[iii])
                wire.append([b,a])
                port_text += f'\t.{input[i][1]}  ({a})'
                port_text += ',\n'
            port_text += '\t// output\n'
            for i in range(output.shape[0]):
                a = self.re_do(output[i][1],compile_text)
                b = output[i][0]
                bb = re.findall(r'(\w+)',output[i][0]) 

                for iii in range(parameter.shape[0]) :
                    if parameter[iii] in bb :
                        b=b.replace(parameter[iii],parameter_c[iii])

                wire.append([b,a])
                port_text += f'\t.{output[i][1]}  ({a})'
                if i == output.shape[0]-1 :
                    port_text += '\n);\n'
                else:
                    port_text += ',\n'

            text += f'{data[instant_index,0]} {parameter_text} {data[instant_index,1]} {port_text} \n'
 
        return text,wire

    def re_do(self,in_text,in_compile):
        compile_list = re.findall(r'(\S+)\s*:\s*(\S+)',in_compile)
        if len(compile_list) > 0:
            for compile_a in compile_list:
                [str,num] = re.subn('^'+compile_a[0]+'$',compile_a[1],in_text,1)
                if num > 0:
                    break
        else:
            str = in_text
        return str


    def gen_text_wirereg(self,mode_info,wire):
        text = ''
        reg_list = np.array(mode_info.reg)
        if len(reg_list) >0 :
            a_list,a_index,a_cnt  = np.unique(reg_list[:,1], return_index = True,return_counts = True)

            for i_cnt in range(a_index.size):
                w=reg_list[a_index[i_cnt],0]
                n=reg_list[a_index[i_cnt],1]
                text += f'reg [{w}-1:0] {n};\n'
                if a_cnt[i_cnt] >1:
                    print(f'warning:信号\'{n}\'重复定义！')


        if len(wire)>0:
            wire_list = np.array(wire)
            a_list,a_index,a_cnt  = np.unique(wire_list[:,1], return_index = True,return_counts = True)

            for i_cnt in range(a_index.size):
                i_w = wire[a_index[i_cnt]]
                if reg_list.shape[0]==0 or  i_w[1] not in reg_list[:,1] :
                    if a_cnt[i_cnt] >1:
                        print(f'warning:信号\'{i_w[1]}\'重复定义！')
                    if len(i_w[0]) == 0:
                        w = 1
                        n = i_w[1]
                        text += f'wire [{int(w)-1}:0] {n};\n'
                    elif i_w[0].find(']') !=-1:
                        n = i_w[1]
                        text += f'wire {i_w[0]} {n};\n'
                    elif re.search('^\s*\d+\s*$',i_w[0]):
                        w = i_w[0]
                        n = i_w[1]
                        text += f'wire [{int(w)-1}:0] {n};\n'
        return text

